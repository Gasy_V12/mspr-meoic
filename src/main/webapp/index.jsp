<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Application de test</title>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  
<!--    <body>
  <h1>Bienvenue sur la boutique RCR !</h1>
  	</br>
  	<h2><a href="./products"> <button>Liste des produits</button></a><h2>
  </body>
-->

<body>

<header>
  <h2> Application de test</h2>

</header>
  <div class="topnav">
  	<a class="active" href="./">Application de test</a>
  	<a href="./products">Page de test</a>
  	<a href="./about.html">A propos</a>
</div>

<section>

  
  <article>
    <h1>Bienvenue sur l'application de test</h1>
    <p> Ce site web sert de démonstration pour l'environement d'intégration continue.</p>
  </article>
</section>

</body>
</html>