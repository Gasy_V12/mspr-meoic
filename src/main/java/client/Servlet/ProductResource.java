package client.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import client.Entity.Product;
import client.Service.ProductService;

@Path("/products")
public class ProductResource extends HttpServlet {
	//@EJB
	private ProductService productService = new ProductService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	    response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    out.println("<!DOCTYPE html>\r\n" + 
	    		"<html>\r\n" + 
	    		"  <head>\r\n" + 
	    		"  	<meta charset=\"UTF-8\">\r\n" + 
	    		"    <title>Page web de test</title>\r\n" + 
	    		"    <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">\r\n" + 
	    		"  </head>");
	    out.println("<body>");
	    out.println("<header>\r\n" + 
	    		"  <h2>Page web de test</h2>\r\n" + 
	    		"</header>\r\n");
	    out.println("<div class=\"topnav\">\r\n" + 
	    		"  	<a class=\"active\" href=\"./\">Application de test</a>\r\n" + 
	    		"  	<a href=\"./products\">Page de test</a>\r\n" + 
	    		"  	<a href=\"./about.html\">A propos</a>\r\n" + 
	    		"</div>");
	    out.println("</body>");
	    out.println("</html>");
	}

	@GET
	@Produces("application/json")
	public List<Product> getPieces() {
	return productService.getProductList();
	}

}
